package threadpool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPool {
    private final int tpCapacity;
    private AtomicInteger freeThreads;
    private AtomicInteger runningTasks;
    private AtomicInteger waitingTasks;

    private final WorkerThread[] threads;
    private final LinkedBlockingQueue<Runnable> taskQueue;


    public ThreadPool(int tpCapacity) {
        this.tpCapacity = tpCapacity;
        this.freeThreads = new AtomicInteger(tpCapacity);
        this.runningTasks = new AtomicInteger(0);
        this.waitingTasks = new AtomicInteger(0);
        taskQueue = new LinkedBlockingQueue<Runnable>();
        threads = new WorkerThread[tpCapacity];

        for (int i = 0; i < tpCapacity; i++) {
            threads[i] = new WorkerThread();
            threads[i].start();
        }
    }

    public void execute(Runnable task) {
        waitingTasks.incrementAndGet();
        taskQueue.offer(task);
    }




    public void  displayStats() {
        System.out.println("ThreadPool{" +
                "tpCapacity=" + tpCapacity +
                ", freeThreads=" + freeThreads +
                ", runningTasks=" + runningTasks +
                ", waitingTasks=" + waitingTasks +
                '}');
    }

    private class WorkerThread extends Thread {
        public void run() {
            Runnable task = null;

            while (true) {
                    try {
                        task = taskQueue.take();
                    } catch (InterruptedException ie) {
                        System.out.print("Exception while waiting for task. Exiting..");
                    }
                freeThreads.decrementAndGet();
                runningTasks.incrementAndGet();
                waitingTasks.decrementAndGet();

                task.run();

                freeThreads.incrementAndGet();
                runningTasks.decrementAndGet();
            }
        }
    }
}



