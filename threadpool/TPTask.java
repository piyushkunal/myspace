package threadpool;

/**
 * Created by Piyush Kunal on 07/03/17.
 */

public class TPTask implements Runnable {
    int threadId;

    public TPTask(int threadId) {
        this.threadId = threadId;
    }

    @Override
    public void run()  {
        for(int i = 0; i < 10; i++) {
            System.out.println("Thread: " + threadId + " running for loop number: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                System.out.println("Thread: "+threadId+" interrupted. Exiting");
            }
        }
    }
}

