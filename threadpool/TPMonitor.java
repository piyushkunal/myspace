package threadpool;

/**
 * Created by Piyush Kunal on 07/03/17.
 */
public class TPMonitor implements Runnable {
    private ThreadPool tp;

    public TPMonitor(ThreadPool tp) {
        System.out.println("Starting the thread-pool monitor..");
        this.tp = tp;
    }

    @Override
    public void run() {
        while(true) {
            tp.displayStats();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ie) {
                System.out.println("Monitor interrupted. Exiting...");
            }
        }
    }
}
