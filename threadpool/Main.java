package threadpool;

/**
 * Created by Piyush Kunal on 07/03/17.
 */
public class Main {
    public static void main(String[] ar) {
        // Create a thread pool of 5 threads
        ThreadPool tp = new ThreadPool(10);

        // Start a thread-pool monitor to view the thread pool stats
        TPMonitor tpMonitor = new TPMonitor(tp);

        // Execute the tp monitor also through Threadpool
        tp.execute(tpMonitor);

        // Create 20 Tasks and submit all of them to thread-pool. View the console for the stats.
        long starttime = System.currentTimeMillis();
        for(int i = 0; i <= 20;i++) {
            TPTask tpTask = new TPTask(i);
            tp.execute(tpTask);
        }
    }
}
