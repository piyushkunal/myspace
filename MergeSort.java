import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Piyush Kunal on 26/12/16.
 */
public class MergeSort {

    static void merge(int start1, int end1, int start2, int end2, int[] arr) {
        int i = start1;
        int j = start2;
        int[] finalArray = new int[end2-start1+1];
        int k = 0;
        while(i <= end1 && j <= end2) {
            if(arr[i] <= arr[j]) {
                finalArray[k] = arr[i];
                i++;
            } else {
                finalArray[k] = arr[j];
                j++;
            }
            k++;
        }
        if(i <= end1) {
            while(i <= end1) {
                finalArray[k] = arr[i];
                i++;k++;
            }
        } else if(j <= end2) {
           while(j <= end2) {
               finalArray[k] = arr[j];
               j++;k++;
           }
        }
        for(i=start1,k=0;i<=end2;i++,k++) {
            arr[i] = finalArray[k];
        }

    }

    static void mergeSort(int start, int end, int[] arr) {
        if(start == end) {
            return;
        }
        int mid = (start+end)/2;
        mergeSort(start, mid, arr);
        mergeSort(mid+1,end,arr);
        merge(start, mid, mid+1,end,arr);
    }

    static void display(int start, int end, int[] arr) {
        System.out.println("\n");
        for(int i=start;i<=end;i++) {
            System.out.print(" "+arr[i]);
        }
    }

//    public static void main(String ar[]) {
//        int[] arr = {3,5,1,7,19,5,3,32,34,43,12,56,1898,34,21,2334,56,43,2334,12,34,56,7};
//        display(0, arr.length-1,arr);
//        mergeSort(0, arr.length -1, arr);
//        display(0, arr.length-1,arr);
//
//    }



    public static void main(String[] args) throws Exception {
        try {

            LinkedBlockingQueue<String> queue = new LinkedBlockingQueue();
            queue.add("");



            File file = new File("/Users/15295/Downloads/boost.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            List<String> stringBuffer = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.add(line);
                //stringBuffer.append(",");
            }
            fileReader.close();
            //System.out.println("Contents of file:");
            System.out.println(stringBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
