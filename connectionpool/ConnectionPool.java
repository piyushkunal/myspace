package connectionpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Piyush Kunal on 10/03/17.
 */
public class ConnectionPool {

    private int maxConnections;
    private AtomicInteger freeConnections;
    private AtomicInteger usedConnections;
    private LinkedBlockingQueue<Connection> connections;

    private static final String db_host = "jdbc:mysql://localhost/myntra";
    private static final String db_user = "root";
    private static final String db_password = "";




    public ConnectionPool(int maxConnections) {
        this.maxConnections = maxConnections;
        this.freeConnections = new AtomicInteger(maxConnections);
        this.usedConnections = new AtomicInteger(0);
        this.connections = new LinkedBlockingQueue<Connection>(maxConnections);
    }

    public void init() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        for(int i = 0; i < maxConnections; i++) {
            Connection conn = DriverManager.getConnection(db_host, db_user, db_password);
            connections.offer(conn);
        }
    }

    public void displayStats() {
        System.out.println("ConnectionPool{" +
                "freeConnections=" + freeConnections.get() +
                ", usedConnections=" + usedConnections.get() +
                ", maxConnections=" + maxConnections +
                '}');
    }

    public Connection borrowConnection(boolean waitForConn) throws  BorrowConnException {
        try {
            if(!waitForConn && connections.isEmpty()) {
                throw new BorrowConnException();
            }
            Connection conn =  connections.take();
            freeConnections.decrementAndGet();
            usedConnections.incrementAndGet();
            return conn;
        } catch (InterruptedException ie) {
            System.out.print("Interrupted queue. Exiting..");
            throw new BorrowConnException();
        }
    }

    public void destroyConnection(Connection connection) {
        if(connection == null) {
            return;
        }
        connections.offer(connection);
        freeConnections.incrementAndGet();
        usedConnections.decrementAndGet();
    }

    public void shutDown() {
        while(!connections.isEmpty()) {
            Connection conn = connections.poll();
            usedConnections.decrementAndGet();
            try {
                conn.close();
            } catch (SQLException se) {
                System.out.println("Error in shutting down connection");
            }
        }
        maxConnections = 0;
        freeConnections = new AtomicInteger(0);
        usedConnections = new AtomicInteger(0);
    }
}
