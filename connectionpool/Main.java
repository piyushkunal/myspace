package connectionpool;

import threadpool.TPMonitor;
import threadpool.ThreadPool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Piyush Kunal on 10/03/17.
 */
public class Main {

    public static void main(String[] ar) throws SQLException, ClassNotFoundException, InterruptedException {
        ConnectionPool connectionPool = new ConnectionPool(3);
        ThreadPool threadPool = new ThreadPool(5);

        TPMonitor tpMonitor = new TPMonitor(threadPool);
        CPMonitor cpMonitor = new CPMonitor(connectionPool);

        threadPool.execute(tpMonitor);
        threadPool.execute(cpMonitor);

        connectionPool.init();

        for(int i = 0; i< 10; i++) {
            QueryTask queryTask = new QueryTask(i, connectionPool);
            threadPool.execute(queryTask);
        }
    }
}
