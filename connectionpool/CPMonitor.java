package connectionpool;

/**
 * Created by Piyush Kunal on 10/03/17.
 */
public class CPMonitor implements Runnable {

    private ConnectionPool connectionPool;

    public CPMonitor(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public void run() {
        while(true) {
            connectionPool.displayStats();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ie) {
                System.out.println("Monitor interrupted. Exiting...");
            }
        }
    }
}
