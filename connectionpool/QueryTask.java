package connectionpool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by Piyush Kunal on 10/03/17.
 */
public class QueryTask implements Runnable {
    private int threadId;
    private ConnectionPool connectionPool;

    public QueryTask(int threadId, ConnectionPool connectionPool) {
        this.threadId = threadId;
        this.connectionPool = connectionPool;
    }

    @Override
    public void run() {
        Connection connection = null;
        try {
            System.out.println("Thread: " + threadId + " going to borrow connection");
            connection = connectionPool.borrowConnection(true);
            try {
                for (int i = 0; i < 5; i++) {
                    Statement stmt = connection.createStatement();
                    String sql = "select * from mystique_config";
                    ResultSet rs = stmt.executeQuery(sql);
                    System.out.println("Thread: "+threadId+" and Query number: "+i+" Result is: "+rs.toString());
                    Thread.sleep(500);
                }
                Thread.sleep(1000);
            } catch(Exception e){
                System.out.println("Thread: " + threadId + " exception while running query. " + e.getMessage());
            }
        } catch (BorrowConnException bce) {
            System.out.println("Thread: "+threadId+" Connection is not available..");
        } finally {
            connectionPool.destroyConnection(connection);
        }



    }
}
