package hashmap;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Piyush Kunal on 16/03/17.
 */
public class HashMap<K,V> implements Map<K,V>{

    private Entry[] listEntries;
    private int size = 15;

    public HashMap() {
        listEntries = new Entry[size];
    }

    private int hashCode(K key) {
        return Math.abs(key.hashCode()) % size;
    }

    @Override
    public void put(K key, V value) {
        Entry<K,V> entry = new Entry(key, value, null);
        int index = this.hashCode(key);
        Entry<K,V> head = listEntries[index];
        if(head == null) {
            head = entry;
            listEntries[index] = head;
            return;
        }
        Entry prev = null;
        while(head != null) {
            if (entry.key.equals(head.key)) {
                if (prev == null) {
                    listEntries[index] = entry;
                    return;
                }
                entry.next = head.next;
                prev.next = entry;
                return;
            }
            prev = head;
            head = head.next;
        }
        prev.next = entry;
    }

    @Override
    public V get(K key) {
        int index = this.hashCode(key);
        Entry<K,V> head = listEntries[index];
        while(head != null) {
            if(head.key.equals(key)) {
                return head.value;
            }
            head = head.next;
        }
        return null;
    }

    @Override
    public Set<K> keyset() {
        Set<K> keySet = new HashSet<K>();
        for(int i = 0; i < listEntries.length; i++) {
            Entry<K, V> head = listEntries[i];
            if (head == null) {
                continue;
            }
            while (head != null) {
                keySet.add(head.key);
                head = head.next;
            }
        }
        return keySet;
    }

    @Override
    public boolean remove(K key) {
        int index = this.hashCode(key);
        Entry<K,V> head = listEntries[index];
        Entry prev = null;
        while(head != null) {
            if(head.key.equals(key)) {
                if(prev == null) {
                    listEntries[index] = null;
                    return true;
                }
                prev.next = head.next;
                return true;
            }
            prev = head;
            head = head.next;
        }
        return false;
    }

    private static class Entry<K,V> {
        K key;
        V value;
        Entry<K,V> next;

        public Entry(K key, V value, Entry<K,V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
