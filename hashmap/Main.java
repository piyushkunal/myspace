package hashmap;

/**
 * Created by Piyush Kunal on 16/03/17.
 */
public class Main {
    public static void main(String[] ar) {
        Map<String,Integer> mymap = new HashMap<String, Integer>();
        mymap.put("Kunal",1);
        mymap.put("piyush",3);
        System.out.println(mymap.keyset());
        System.out.println(mymap.get("kunal"));
        System.out.println(mymap.get("Kunal"));
        mymap.put("Kunal",145);
        System.out.println(mymap.get("Kunal"));
        mymap.put("Kunal1",146);
        mymap.put("Kunal2",147);
        mymap.put("Kunal3",148);

        for(String key: mymap.keyset()) {
            System.out.println("key: "+key+" and value: "+mymap.get(key));
        }
        System.out.println(mymap.keyset());
    }
}
