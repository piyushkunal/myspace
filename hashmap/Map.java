package hashmap;

import java.util.Set;

/**
 * Created by Piyush Kunal on 16/03/17.
 */
public interface Map<K,V> {

    void put(K key, V value);
    V get(K key);
    Set<K> keyset();
    boolean remove(K key);
}
